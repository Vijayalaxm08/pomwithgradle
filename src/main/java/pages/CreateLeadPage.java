package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {

	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="createLeadForm_companyName")
	private WebElement eleCompanyName;
	public CreateLeadPage typeCompanyName(String companyName) {
		type(eleCompanyName, companyName);
		return this;
	}
	
	@FindBy(id="createLeadForm_firstName")
	private WebElement eleFirstName;
	public CreateLeadPage typeFirstName(String firstName) {
		type(eleFirstName, firstName);
		return this;
	}
	
	@FindBy(id="createLeadForm_lastName")
	private WebElement eleLastName;
	public CreateLeadPage typeLastName(String lastName) {
		type(eleLastName, lastName);
		return this;
	}
	
	@FindBy(name="dataSourceId")
	private WebElement eleSouce;
	public CreateLeadPage selectSourceId(String sourceId) {
		selectDropDownUsingText(eleSouce,sourceId);
		return this;
	}
	
	@FindBy(id="createLeadForm_marketingCampaignId")
	private WebElement eleCampId;
	public CreateLeadPage selectCampaignId(String campaignId) {
		selectDropDownUsingText(eleCampId,campaignId);
		return this;
	}
	
	@FindBy(id="createLeadForm_firstNameLocal")
	private WebElement elefNameLocal;
	public CreateLeadPage typeFirstNameLocal(String fNameLocal) {
		type(elefNameLocal,fNameLocal);
		return this;
	}
	
	@FindBy(id="createLeadForm_lastNameLocal")
	private WebElement elelNameLocal;
	public CreateLeadPage typeLastNameLocal(String fLastLocal) {
		type(elelNameLocal,fLastLocal);
		return this;
	}
	
	@FindBy(id="createLeadForm_personalTitle")
	private WebElement eleSalutation;
	public CreateLeadPage typeSalutation(String salutation) {
		type(eleSalutation,salutation);
		return this;
	}
	
	@FindBy(id="createLeadForm_generalProfTitle")
	private WebElement eleTitle;
	public CreateLeadPage typeTitle(String title) {
		type(eleTitle,title);
		return this;
	}
	
	@FindBy(id="createLeadForm_departmentName")
	private WebElement eleDeptName;
	public CreateLeadPage typeDeptName(String deptName) {
		type(eleDeptName,deptName);
		return this;
	}
	
	@FindBy(id="createLeadForm_annualRevenue")
	private WebElement eleAnnualRevenue;
	public CreateLeadPage typeAnnualRevenue(String annualRevenue) {
		type(eleAnnualRevenue,annualRevenue);
		return this;
	}
	
	@FindBy(id="createLeadForm_currencyUomId")
	private WebElement eleCurrencyID;
	public CreateLeadPage selectCurrency(String currency) {
		selectDropDownUsingText(eleCurrencyID,currency);
		return this;
	}
	
	@FindBy(id="createLeadForm_industryEnumId")
	private WebElement eleEnumId;
	public CreateLeadPage selectEnumId() {
		selectDropDownUsingIndex(eleEnumId,3);
		return this;
	}
	
	@FindBy(id="createLeadForm_numberEmployees")
	private WebElement eleNoOfEmployees;
	public CreateLeadPage typeNoOfEmployees() {
		type(eleNoOfEmployees,"50");
		return this;
	}
	
	@FindBy(id="createLeadForm_ownershipEnumId")
	private WebElement eleOwnership;
	public CreateLeadPage selectOwnerShipEnumId() {
		selectDropDownUsingText(eleOwnership,"Partnership");
		return this;
	}
	
	@FindBy(id="createLeadForm_sicCode")
	private WebElement eleSICCode;
	public CreateLeadPage typeSICCode() {
		type(eleSICCode,"772577");
		return this;
	}
	
	@FindBy(id="createLeadForm_tickerSymbol")
	private WebElement eleTickerSymbol;
	public CreateLeadPage typeTickerSymbol() {
		type(eleTickerSymbol,"Yes");
		return this;
	}
	
	@FindBy(id="createLeadForm_primaryEmail")
	private WebElement eleEmailId;
	public CreateLeadPage typeEmailID() {
		type(eleEmailId,"vijayalaxmi.thilaga@gmail.com");
		return this;
	}
	
	@FindBy(id="createLeadForm_primaryPhoneNumber")
	private WebElement elePhoneNumber;
	public CreateLeadPage typePhoneNumber() {
		type(elePhoneNumber,"9003228239");
		return this;
	}
	
	@FindBy(id="createLeadForm_description")
	private WebElement eleDescription;
	public CreateLeadPage typeDescription() {
		type(eleDescription,"Creation of Create Lead");
		return this;
	}
	
	@FindBy(className="smallSubmit")
	private WebElement eleCreateLeadButton;
	public ViewLeadPage clickCreateLeadButton() {
		click(eleCreateLeadButton);
		return new ViewLeadPage();
	}
	
	
}





