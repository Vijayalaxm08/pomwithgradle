package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class DuplicateLeadPage extends ProjectMethods {

	public DuplicateLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	public DuplicateLeadPage verifyPageTitle() {
		verifyTitle("Duplicate Lead | opentaps CRM");
		return this;
	}
	
	@FindBy(xpath="//input[@value='Create Lead']")
	private WebElement eleCreateLeadButton;
	public ViewLeadPage clickCreateLeadButton_DuplicateLead() {
		//WebElement eleCrm = locateElement("link", "CRM/SFA");
		click(eleCreateLeadButton);
		return new ViewLeadPage();
	}
	
}





