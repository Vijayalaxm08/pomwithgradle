package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods {

	public String leadID=null;
	public static String text;
	public FindLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="(//input[@name='firstName'])[3]")
	private WebElement eleFirstName;
	public FindLeadsPage typeFNameFindLead() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		type(eleFirstName, "Vijayalaxmi");
		return this;
	}
	
	@FindBy(xpath="//button[text()='Find Leads']")
	private WebElement eleFindLeadsButton;
	public FindLeadsPage clickFindLeadsButton() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleFindLeadsButton);
		return this;
	}
	
	@FindBy(xpath="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a")
	private WebElement firstLeadID;
	public ViewLeadPage clickFirstLeadID() {
		click(firstLeadID);
		return new ViewLeadPage();
	}
	
	
	@FindBy(linkText="Edit")
	private WebElement eleEdit;
	public EditPage clickEditLink() {
		click(eleEdit);
		return new EditPage();
		
	}
	
	@FindBy(xpath="(//span[@class='x-tab-strip-text '])[3]")
	private WebElement eleEmail;
	public FindLeadsPage clickEmailTab() {
		click(eleEmail);
		return this;
	}
	
	@FindBy(name="emailAddress")
	private WebElement eleEmailId;
	public FindLeadsPage typeEmailId(String emailID) {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		type(eleEmailId,emailID);
		return this;
	}
	
	@FindBy(xpath="(//span[@class='x-tab-strip-text '])[2]")
	private WebElement elePhone;
	public FindLeadsPage clickPhoneTab() {
		click(elePhone);
		return this;
	}
	
	@FindBy(name="phoneNumber")
	private WebElement elePhoneNumber;
	public FindLeadsPage typePhoneNumber(String phoneNumber) {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		type(elePhoneNumber,phoneNumber);
		return this;
	}
	
	@FindBy(xpath="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a")
	private WebElement eleFirstID;
	public FindLeadsPage getFirstLeadID() {
		text = getText(eleFirstID);
		System.out.println(text);
		return new FindLeadsPage();
		//return text;
	}
	
	//@FindBy(xpath="(//input[@class=' x-form-text x-form-field '])[1]")
	
	//@FindBy(xpath="(//input[contains(@class,'x-form-text x-form-field')])[28]")
	//private WebElement eleLeadID;
	public FindLeadsPage typeLeadID() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			
		}
		System.out.println(text);
		String leadId=text;
		driver.findElementByXPath("(//input[contains(@class,'x-form-text x-form-field')])[28]").sendKeys(leadId);
		return this;
	}
	
	@FindBy(className="x-paging-info")
	private WebElement eleErrMsg;
	public FindLeadsPage verifyErrorMessage() {
		verifyExactText(eleErrMsg, "No records to display");	
		return this;
	}
	
}





