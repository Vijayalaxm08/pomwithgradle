package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class LeadPage extends ProjectMethods {

	public LeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(linkText="Create Lead")
	private WebElement eleCreateLead;
	public CreateLeadPage clickCreateLead() {
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	
	@FindBy(linkText="Find Leads")
	private WebElement eleFindLeads;
	public FindLeadsPage clickFindLeadsLink() {
		click(eleFindLeads);
		return new FindLeadsPage();
	}
	
}





