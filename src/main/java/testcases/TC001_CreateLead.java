package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.CreateLeadPage;
import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC001_CreateLead extends ProjectMethods{
  
	@BeforeTest
	public void setData() {
		testCaseName="TC001_CreateLead";
		testDescription="Creating a Lead";
		authors="Gayatri";
		category="Smoke";
		testNodes ="Leads";
		dataSheetName="TC001_CreateLead";
	}
	
	@Test(dataProvider="fetchData")
	public void createLead(String uname, String pwd, String compName, String fName, String lName, 
	String sourceId, String campaignId, String fNameLocal, String fLastLocal, String salutation,
	String title, String deptName, String annualRevenue, String currency) {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.crmsfaLink()
		.clickLeads()
		.clickCreateLead()
		.typeCompanyName(compName)
		.typeFirstName(fName)
		.typeLastName(lName)
		.selectSourceId(sourceId)
		.selectCampaignId(campaignId)
		.typeFirstNameLocal(fNameLocal)
		.typeLastNameLocal(fLastLocal)
		.typeSalutation(salutation)
		.typeTitle(title)
		.typeDeptName(deptName)
		.typeAnnualRevenue(annualRevenue)
		.selectCurrency(currency)
		.selectEnumId()
		.typeNoOfEmployees()
		.selectOwnerShipEnumId()
		.typeSICCode()
		.typeTickerSymbol()
		.typeEmailID()
		.typePhoneNumber()
		.typeDescription()
		.clickCreateLeadButton()
		.verifyFirstName();
	
	}

	
}
