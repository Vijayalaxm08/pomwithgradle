package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC001_LoginAndLogout extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName="TC001_LoginAndLogout";
		testDescription="Login into leaftaps";
		authors="Gayatri";
		category="Smoke";
		testNodes ="Leads";
		dataSheetName="TC001";
	}
	@Test(dataProvider="fetchData")
	public void loginAndLogout(String uname, String pwd) {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.clickLogout();
		
	}
}
