package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.CreateLeadPage;
import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC003_DuplicateLead extends ProjectMethods{
  
	@BeforeTest
	public void setData() {
		testCaseName="TC003_DuplicateLead";
		testDescription="Editing the Lead";
		authors="Gayatri";
		category="Smoke";
		testNodes ="Leads";
		dataSheetName="TC003_DuplicateLead";
	}
	
	@Test(dataProvider="fetchData")
	public void editLead(String uname, String pwd, String emailID) {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.crmsfaLink()
		.clickLeads()
		.clickFindLeadsLink()
		.clickEmailTab()	
		.typeEmailId(emailID)
		.clickFindLeadsButton()
		.clickFirstLeadID()
		.clickDuplicateLeadButton()
		.verifyPageTitle()
		.clickCreateLeadButton_DuplicateLead()
		.verifyFirstName();
		
	}
	
}
